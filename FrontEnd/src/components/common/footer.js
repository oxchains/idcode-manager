/**
 * oxchain ivoice app
 *
 *
 * Author: Jun
 * Date: 13/04/2017
 *
 */

import React from 'react';

export default () => {
  return (
    <footer className="main-footer">
      <div className="pull-right hidden-xs">
        <b>Version</b> 0.1.0
      </div>
      <strong>Copyright &copy; 2018 <a href="" target="_blank">全球码链服务联盟</a>.</strong> All rights
      reserved.&nbsp;&nbsp;
    </footer>
  );
}
