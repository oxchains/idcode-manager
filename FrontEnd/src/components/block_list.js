/**
 * oxchain ivoice app
 *
 *
 * Author: Jun
 * Email: iyakexi@gmail.com
 * Date: 05/05/2017
 *
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchBlockList, fetchBlockInfo, fetchBlockQuery} from '../actions/chain';
import {Link} from 'react-router-dom';
import {
    Modal,
    ModalHeader,
    ModalTitle,
    ModalClose,
    ModalBody,
    ModalFooter
} from 'react-modal-bootstrap';


class BlockList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDetailModalOpen: false,
            selectedId: null,
            queryClick: false,
            value: "block",
            showcontent: '',
            num: ''
        };
        this.handleQuery = this.handleQuery.bind(this);
        this.selectValue = this.selectValue.bind(this);
        this.renderRows = this.renderRows.bind(this);

    }

    componentWillMount() {
        this.props.fetchBlockList(this.props.chainName);
    }

    handleQuery() {
        const num = this.refs.num.value;
        const chainname = this.props.chainName;
        const value = this.state.value;
        if (num.length === 0) {
            alert('您输入的值为空');
        } else if (value === 'trade' && num.length !== 64) {
            alert('请输入正确的交易ID');
        } else {
            this.setState({
                queryClick: true,
                showcontent: this.state.value,
                num: this.refs.num.value
            });
            localStorage.setItem("txid", this.refs.num.value);
            this.props.fetchBlockQuery({chainname, num}, err => {
            })
        }
    }

    selectValue(e) {
        const value = e.target.value;
        this.setState({
            value: value
        });
    }

    renderRows() {
        const value = this.state.showcontent;
        const num = this.state.num;
        const name = this.props.chainName;
        const queryClick = this.state.queryClick;
        const querydata = this.props.queryblock;
        if (queryClick === false) {
            localStorage.setItem("txid", null);
            return this.props.blocks.map((row, idx) => {
                // console.log(row);
                return (<tr key={idx}>
                    <td>{row.number}</td>
                    <td>{row.size}</td>
                    <td>{row.hash}</td>
                    <td>{row.previous}</td>
                    <td>
                        <button className={`btn btn-sm warning margin-r-5 hidden`}
                                onClick={this.handleDetailClick.bind(this, row.number)}>详情
                        </button>
                        <Link className="btn btn-sm warning" to={`/chain/${name}/block/${row.number}`}>详情</Link>
                    </td>
                </tr>);
            });
        } else if (queryClick === true) {
            localStorage.setItem("txid", this.refs.num.value);
            if (querydata) {
                return (<tr>
                    <td>{querydata.number}</td>
                    <td>{value === 'trade' ? num : querydata.size}</td>
                    <td>{querydata.hash}</td>
                    <td>{querydata.previous}</td>
                    <td>
                        <button className={`btn btn-sm warning margin-r-5 hidden`}
                                onClick={this.handleDetailClick.bind(this, querydata.number)}>详情
                        </button>
                        <Link className="btn btn-sm warning" to={`/chain/${name}/block/${querydata.number}`}>详情</Link>
                    </td>
                </tr>);
            } else if (querydata === null) {
                return (
                    <tr>
                        <td colSpan='5' className="content text-center">
                            没有查询到此区块的信息
                        </td>
                    </tr>
                )
            }

        }
    }

    handleDetailClick(index) {
        this.props.fetchBlockInfo(this.props.chainName, index);
        this.setState({isDetailModalOpen: true});
    }

    render() {

        if (this.props.blocks === null) {
            return <div>
                <section className="content"><h3>Loading...</h3></section>
            </div>
        }
        const value = this.state.showcontent;

        return (
            <div>
                <section className="text-center">
                    <div action="" className="form-group">
                        <select value={this.state.value} className="form-control" style={{width: 120, marginRight:20}}
                                onChange={this.selectValue}>
                            <option value="block">区块查询</option>
                            <option value="trade">交易查询</option>
                        </select>

                        <input className="form-control g-ml-20 margin-r-10" style={{width: 450}} type="text" ref='num'
                               placeholder={` ${this.state.value === "block" ? "请输入要查询的区块号或区块哈希值" : "请输入要查询的交易ID" }  `}/>
                        <input className="btn btn-sm warning block_query" type="submit" value='查询'
                               onClick={this.handleQuery}/>
                    </div>
                </section>
                <section className="content-header"><h1></h1></section>
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box box-info">
                                <div className="box-header">
                                    <h3 className="box-title"> {value === "block" ? "查询结果" : value === "trade" ? "查询结果" : "最新区块"}</h3>
                                    <div className="box-tools pull-right">
                                        <button type="button" className="btn btn-box-tool" data-widget="collapse"><i
                                            className="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <table className="table table-bordered table-hover multi-line-table">
                                        <tbody>
                                        <tr>
                                            <th>区块号</th>
                                            <th>{value === 'trade' ? "查询ID" : "区块大小"} </th>
                                            <th>区块哈希值</th>
                                            <th>上一区块哈希值</th>
                                            <th></th>
                                        </tr>
                                        {this.renderRows()}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="box-footer clearfix">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


            </div>)
    }
}

function mapStateToProps(state) {
    return {
        blocks: state.chain.blocks,
        queryblock: state.chain.queryblock
    };
}

export default connect(mapStateToProps, {fetchBlockList, fetchBlockInfo, fetchBlockQuery})(BlockList);