package oxchains.fabric.console.rest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import oxchains.fabric.console.domain.ChainBlockInfo;
import oxchains.fabric.console.rest.common.RestResp;
import oxchains.fabric.console.service.ChainService;

import java.util.Optional;

import static java.util.Objects.nonNull;
import static oxchains.fabric.console.rest.common.RestResp.fail;
import static oxchains.fabric.console.rest.common.RestResp.success;

/**
 * @author aiet
 */
@RestController
public class FabricChainController {

    private ChainService chainService;

    public FabricChainController(ChainService chainService) {
        this.chainService = chainService;
    }

    @PostMapping("/chain")
    public RestResp newChain(@RequestParam String chainname, @RequestParam("config") MultipartFile config) {
        return chainService.newChain(chainname, config) ? success(null) : fail();
    }

    @GetMapping("/chain")
    public RestResp chains(){
        return success(chainService.chains());
    }

    @GetMapping("/chain/{chainname}")
    public RestResp chaininfo(@PathVariable String chainname) {
        return chainService
          .chainInfo(chainname)
          .map(RestResp::success)
          .orElse(fail());
    }

    @GetMapping("/chain/{chainname}/block")
    public RestResp chainblock(@PathVariable String chainname, @RequestParam(required = false, defaultValue = "-1") long number, @RequestParam(required = false) String tx, @RequestParam(required = false) String hash) {
        Optional<ChainBlockInfo> blockInfoOptional;
        if (nonNull(tx)) {
            blockInfoOptional = chainService.chainBlockByTx(chainname, tx);
        } else if (number > -1) {
            blockInfoOptional = chainService.chainBlockByNumber(chainname, number);
        } else if (nonNull(hash)) {
            blockInfoOptional = chainService.chainBlockByHash(chainname, hash);
        }
        else return success(chainService.chainBlocks(chainname));

        return blockInfoOptional
          .map(RestResp::success)
          .orElse(fail());
    }

    @GetMapping("/chain/{chainname}/block/{num}")
    public RestResp chainblockinfo(@PathVariable String chainname, @PathVariable String num){
        Optional<ChainBlockInfo> blockInfoOptional;
        try{

            if(StringUtils.isEmpty(num)){
                return fail("区块号或者区块哈希不能为空");
            }
            if(num.length() < 64){
                if(Long.valueOf(num) < 0){
                    return fail("区块号必须是正整数");
                }
                blockInfoOptional = chainService.chainBlockByNumber(chainname, Long.valueOf(num));
            }else{
                blockInfoOptional = chainService.chainBlockByHash(chainname, num);
                if(!blockInfoOptional.isPresent()){
                    blockInfoOptional = chainService.chainBlockByTx(chainname, num);
                }
            }
        }catch (NumberFormatException e){
            return fail("区块号必须是正整数");
        }
        return blockInfoOptional
                .map(RestResp::success)
                .orElse(fail("没有数据"));
    }

    @GetMapping("/chain/{chainname}/tx/{tx}")
    public RestResp chainblock(@PathVariable String chainname, @PathVariable String tx) {
        return chainService
          .transaction(chainname, tx)
          .map(RestResp::success)
          .orElse(fail());
    }

    @PostMapping("/chain/{chainname}/peer")
    public RestResp joinChain(@PathVariable String chainname, @RequestParam String peer){
        return chainService.joinChain(chainname, peer) ? success(null) : fail() ;
    }

}
